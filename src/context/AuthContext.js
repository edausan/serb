import { createContext } from 'react';

export const AuthContext = createContext({
  isLoggedin: false,
  user: {},
  login: () => { },
  logout: () => { },
});

// export default AuthContext;
