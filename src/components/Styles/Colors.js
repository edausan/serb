export const Colors = {
  blue: '#4dd0e1',
  blueActive: '#11a9bd',
  pink: '#e7497d',
  red: '#f44336',
  yellow: '#ffeb3b',
  green: '#4caf50',
  gray: '#b2b2b2',
  darkGray: '#393939'
}