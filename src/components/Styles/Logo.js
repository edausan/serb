import React from 'react'
import { css } from 'glamor';

const Logo = ({ inLogin = false, styles = {} }) => {
  return (
    <figure className={logoWrapper(inLogin, styles)}>
      <img src="/static/images/SERB.png" alt="" />
    </figure>
  );
}


const logoWrapper = (inLogin, styles) => css({
  display: 'block',
  margin: 0,
  width: '100%',
  padding: inLogin ? '100px 0' : 0,
  ...styles,
  '> img': {
    width: 215,
    height: 78.5,
    margin: 'auto auto',
    display: 'block'
  }
});

export default Logo;