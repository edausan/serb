import { css } from "glamor";
import { Colors } from "./Colors";

export const mainBackgroundColor = `linear-gradient(to bottom, ${Colors.blue}, ${Colors.pink})`;

export const mergeClasses = (classes = [], styles = {}) => css([...classes], { ...styles });

export const defaultTransition = css({ transition: '250ms ease' });