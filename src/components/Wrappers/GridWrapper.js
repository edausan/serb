import React from 'react'
import { css } from 'glamor';

const GridWrapper = ({
  template = '1fr',
  gap = 10,
  styles = {},
  classes = [],
  children,
  key = '',
  id = '',
}) => {
  const props = { template, gap, styles, classes }

  return (
    <section key={key} data-name="grid-wrapper" id={id} className={wrapper(props)}>
      {children}
    </section>
  );
}

const wrapper = ({ template, gap, styles, classes }) => css([...classes], {
  display: 'grid',
  position: 'relative',
  zIndex: 9999,
  gridTemplateColumns: template,
  gridGap: gap,
  ...styles
});

export default GridWrapper;