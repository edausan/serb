const useValidation = (user, from = 'sign-up') => {
  const _checkEmail = () => {
    let emailValid = []
    if (user) {
      emailValid = user.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    }

    if (emailValid) {
      return false
    } else {
      return true
    }
  }

  const _checkPassword = (passType = 'password') => {
    switch (passType) {
      case 'password':
        if (user.password.length <= 0) {
          return true
        } else if (user.password.length > 0) {
          return false
        }
        break;
      case 'confirm-password':
        if (user.confirm_password.length <= 0) {
          return true
        } else if (user.confirm_password.length > 0) {
          return false
        }
        break;

      default:
        break;
    }
  }

  const formValidation = {
    email: _checkEmail(),
    password: _checkPassword(),
    confirmPassword: from === 'sign-up' ? _checkPassword('confirm-password') : '',
    formValid: !_checkEmail() && !_checkPassword() && from === 'sign-up' ? !_checkPassword('confirm-password') : true
  }



  return formValidation;
}

export default useValidation;