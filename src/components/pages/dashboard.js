import React, { useContext, useEffect } from 'react';
import { AuthContext } from '../../context/AuthContext';
import GridWrapper from '../Wrappers/GridWrapper';
import Button from '../Form/Button';

const Dashboard = () => {
  const ctx = useContext(AuthContext);
  const user = ctx.user;

  useEffect(() => {
    console.log(ctx.isLoggedin);

    if (ctx.isLoggedin === false) {
      window.location.href = '/'
    }
  }, [ctx.isLoggedin]);

  const _logout = () => {
    ctx.logout();
    window.location.href = '/'
  }

  return (
    <GridWrapper>
      <h1>Welcome, {user.email}!</h1>
      <Button text="LOGOUT" onClickHandler={_logout} />
    </GridWrapper>
  );
}

export default Dashboard;