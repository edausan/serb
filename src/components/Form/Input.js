import React, { useState } from 'react'
import { mergeClasses, defaultTransition } from '../Styles'
import { css } from 'glamor';
import GridWrapper from '../Wrappers/GridWrapper';
import { Colors } from '../Styles/Colors';

const InputText = ({
  placeholder = '',
  icon = '',
  value = '',
  type = 'text',
  error = false,
  id = '',
  locked = false,
  onChange = () => { }
}) => {

  const [active, setActive] = useState(false)

  const _onFocusHandler = () => {
    setActive(!active);
  }

  return (
    <GridWrapper template={icon.length > 0 ? '30px 1fr' : '1fr'} gap={0} classes={[wrapper(locked), active ? activeClass : error ? errorClass : '']}>

      {
        icon.length > 0 && <img className={iconClass(active)} src={`/static/icons/${icon}.png`} alt="" />
      }

      <input
        id={id}
        name={id}
        type={type}
        placeholder={placeholder}
        className={mergeClasses([inputClass])}
        onFocus={_onFocusHandler}
        onBlur={_onFocusHandler}
        onChange={onChange}
        value={value}
        disabled={locked}
      />
    </GridWrapper>
  );
}

const wrapper = (locked) => css(defaultTransition, {
  border: `1px solid ${locked ? '#eee3' : 'white'}`,
  borderRadius: 70,
  // padding: '5px 20px',
  alignContent: 'center',
  justifyContent: 'center',
  background: 'rgba(255,255,255,.050)',
  margin: '0 auto',
  width: 295,
  height: 50,
  marginBottom: 15,
  boxSizing: 'border-box',
  '> input': {
    color: locked ? '#eee3 !important' : 'white'
  }
});

const activeClass = css({
  borderColor: Colors.blue,
  background: 'white',
  '> input': {
    color: '#000'
  }
});

const errorClass = css({
  borderColor: Colors.red,
  background: 'white',
  '> input': {
    color: Colors.red,
    '::placeholder': {
      color: Colors.red,
    }
  }
})

const iconClass = active => css(defaultTransition, {
  width: 15,
  display: 'inline-block',
  filter: active ? 'none' : 'brightness(500)',
  margin: 'auto 0',
  marginLeft: 'auto',

});

const inputClass = css(defaultTransition, {
  borderStyle: 'none',
  background: 'none !important',
  padding: 15,
  display: 'block',
  margin: 0,
  color: 'white',
  fontSize: 15,
  ':focus': {
    outline: 'none',
    '::placeholder': {
      color: '#ccc'
    }
  },
  '::placeholder': {
    transition: '250ms ease',
    color: 'white'
  }
})

export default InputText;