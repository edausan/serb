import React from 'react';
import { css } from 'glamor';
import GridWrapper from '../Wrappers/GridWrapper';
import { Colors } from '../Styles/Colors';
import { defaultTransition } from '../Styles';

const StrengthBar = ({ value = 0 }) => {
  let color = Colors.red;
  let label = 'Weak'
  if (value > 6 && value < 12) {
    color = Colors.yellow
    label = 'Average'
  } else if (value >= 12) {
    color = Colors.green
    label = 'Strong'
  }

  return (
    <GridWrapper template="57px 1fr" classes={[wrapper]}>
      <label htmlFor="">{label}</label>
      <div className={barWrapper}>
        <div className={bar(value, color)} />
      </div>
    </GridWrapper>
  );
}

const bar = (value, color) => {
  return css(defaultTransition, {
    background: color,
    height: '100%',
    width: `${value * 8.333333}%`
  })
}

const barWrapper = css({
  border: '1px solid white',
  height: 10,
  borderRadius: 5,
  width: '100%',
  margin: 'auto auto',
  overflow: 'hidden'
});

const wrapper = css({
  width: 245,
  color: 'white',
  margin: '0 auto',
  fontSize: 14,
  alignContent: 'center',
  marginBottom: 10
});

export default StrengthBar;