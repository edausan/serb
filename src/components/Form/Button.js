import React from 'react'
import { css } from 'glamor';
import { defaultTransition } from '../Styles';
import { Colors } from '../Styles/Colors';

const Button = ({
  styles = {},
  text = '',
  onClickHandler = () => { }
}) => {
  return (
    <button onClick={onClickHandler} className={btn(styles)}>
      {text}
    </button>
  );
}

const btn = (styles) => css(defaultTransition, {
  borderStyle: 'none',
  background: Colors.blue,
  color: 'white',
  // padding: '20px 20px',
  display: 'block',
  width: 295,
  height: 50,
  margin: '0 auto',
  boxSizing: 'border-box',
  marginBottom: 15,
  borderRadius: 70,
  fontSize: '1rem',
  fontWeight: 600,
  cursor: 'pointer',
  ...styles,
  ':focus': {
    outline: 'none'
  },
  ':active': {
    background: Colors.blueActive
  }

});

export default Button;