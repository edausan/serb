import React, { useState } from 'react';
import { css } from 'glamor';
import { Colors } from '../Styles/Colors';

const UploadID = ({
  id = "",
  error = false,
  locked = false,
  setFile = () => { },
}) => {
  const [image, setImage] = useState('');
  const reader = new FileReader();

  const _clickFileInput = (e) => {
    const input = document.querySelector(`#${id}`);
    const target = e.target;


    if ((target.nodeName === 'SECTION') || (target.nodeName === 'IMG')) {
      input.click();
      console.log(target.nodeName);
    }
  }

  const _onChangeHandler = (e) => {
    const fd = new FormData();
    const file = e.target.files[0];

    if (file) {
      const filename = file.name;
      reader.readAsDataURL(file);
      reader.onloadend = function () {
        setImage(reader.result)
      }
      fd.append('image', file, filename);
      setFile(id.split('-').join('_'), fd);
    }

  }

  const _clearInput = (e) => {
    const input = document.querySelector(`#${id}`);
    input.value = null
    e.preventDefault();
    setImage('');
    setFile(id.split('-').join('_'), '');
  }

  return (
    <section onClick={(e) => locked ? () => { } : _clickFileInput(e)} className={wrapper(error, locked)}>
      <input onChange={_onChangeHandler} id={id} type="file" className={fileInput} accept="image/*" />
      <img className={icon} src="/static/icons/upload_id_icon.png" alt="" />
      {
        !locked && image && <button onClick={_clearInput} className={closeBtn}>X</button>
      }
      {
        image &&
        <figure className={imageWrapper}>
          <img src={image} alt="" />
        </figure>
      }

    </section>
  );
}

const closeBtn = css({
  borderStyle: 'none',
  background: 'white',
  width: 30,
  height: 30,
  borderRadius: '100%',
  position: 'absolute',
  top: 0,
  right: 0,
  transform: 'translate(50%, -50%)',
  zIndex: 1001,
  fontWeight: 600,
  boxShadow: '0 5px 5px rgba(0,0,0,.1)',
  cursor: 'pointer',
  ':focus': {
    outline: 'none'
  }
});

const imageWrapper = css({
  position: 'absolute',
  top: '50%',
  left: 0,
  transform: 'translateY(-50%)',
  width: '100%',
  height: '100%',
  margin: 0,
  background: 'white',
  borderRadius: 3,
  overflow: 'hidden',
  '> img': {
    position: 'absolute',
    top: '50%',
    left: 0,
    transform: 'translateY(-50%)',
    width: '100%'
  }

});

const wrapper = (err, locked) => css({
  position: 'relative',
  cursor: locked ? 'default' : 'pointer',
  width: 120,
  height: 120,
  border: `1px dashed ${err ? Colors.red : 'white'}`,
  display: 'grid',
  placeContent: 'center',
  margin: 'auto auto',
  borderRadius: 3,
  background: 'rgba(255,255,255,.050)',
  opacity: locked ? .5 : 1
});

const fileInput = css({
  display: 'none'
});

const icon = css({
  width: 28,
  filter: 'brightness(1000)'
});

export default UploadID;