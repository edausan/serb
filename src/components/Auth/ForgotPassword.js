import React from 'react';
import GridWrapper from '../Wrappers/GridWrapper';
import InputText from '../Form/Input';
import Button from '../Form/Button';
import { css } from 'glamor';

const ForgotPassword = () => {
  return (
    <GridWrapper classes={[wrapper]} gap={30} styles={{ placeContent: 'center', textAlign: 'center', color: 'white' }}>
      <h1 style={{ margin: 0, fontSize: 25 }}>Forgot Password</h1>
      <p style={{ margin: 0, marginBottom: 20, fontSize: 13 }}>Enter your email below to receive your<br />password reset instructions</p>

      <form action="">
        <InputText type="email" placeholder="Email Address" />
        <Button text="CONTINUE" />
      </form>
    </GridWrapper>
  );
}


const wrapper = css({
  minHeight: '100vh'
})

export default ForgotPassword;