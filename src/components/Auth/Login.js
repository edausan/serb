/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect, useContext } from 'react';
import Input from '../Form/Input';
import Logo from '../Styles/Logo';
import Button from '../Form/Button';
import GridWrapper from '../Wrappers/GridWrapper';
import { css } from 'glamor';
import { Colors } from '../Styles/Colors';
import { Link } from 'react-router-dom'
import useValidation from '../custom-hooks/useValidation';
import { AuthContext } from '../../context/AuthContext';

const Login = () => {
  const formErrorState = {
    email: false,
    password: false,
    formValid: true,
    showMessage: false
  }

  const [user, setUser] = useState({ email: '', password: '' });
  const [formError, setformError] = useState(formErrorState);
  const [formMessage, setFormMessage] = useState('');
  const auth = useContext(AuthContext);
  const localUser = auth.user ? auth.user : { email: '', password: '' };

  useEffect(() => {
    if (auth.isLoggedin) {
      window.location.href = '/dashboard';
    }

  }, [auth.isLoggedin])

  const _onChangeHandler = e => {
    const id = e.target.id;
    setUser({ ...user, [id]: e.target.value });
    setformError(useValidation(user, 'login'));
  }

  const _onSubmit = (e) => {
    e.preventDefault();
    setformError(useValidation(user, 'login'));
    _userValidation();
  }

  const _userValidation = () => {
    if ((localUser.email !== user.email) && (localUser.password !== user.password)) {
      setformError({ ...formError, email: true, password: true, showMessage: true });
      setFormMessage(`Account doesn't exist. Please register first.`);
    } else if ((localUser.email === user.email) && (localUser.password === user.password)) {
      window.location.href = '/dashboard'
      setformError({ ...formError, email: false, password: false, formValid: true, showMessage: false });
      localStorage.setItem('loggedin', true);
    } else if ((localUser.email === user.email) && (localUser.password !== user.password)) {
      setformError({ ...formError, password: true, formValid: false, showMessage: true });
      setFormMessage(`Password doesn't match.`);
    } else if ((localUser.email !== user.email) && (localUser.password === user.password)) {
      setformError({ ...formError, email: true, formValid: false, showMessage: true });
      setFormMessage(`Email doesn't match.`);
    }
  }

  return (
    <GridWrapper>
      <Logo inLogin />
      <form onSubmit={_onSubmit}>
        <Input error={formError.email} id="email" icon="user" type="email" placeholder="Email Address" value={user.email} onChange={_onChangeHandler} />
        <Input error={formError.password} id="password" icon="password" type="password" placeholder="Password" value={user.password} onChange={_onChangeHandler} />

        {
          formError.showMessage &&
          <section>
            <strong style={{ color: 'white' }}>{formMessage}</strong>
          </section>
        }

        <Button text="LOG IN" />

        <div className={forgotRegisterWrapper}>
          <Link to="/forgotpassword">
            <span className={anchor}>Forgot Password?</span>
          </Link>
          <p style={{ fontSize: 14 }}>No account yet? {" "}
            <Link to="/signup">
              <span className={`${anchor} ${register}`}>Register here</span>
            </Link>
          </p>
        </div>
      </form>
    </GridWrapper>
  );
}

const register = css({
  marginTop: 30
});

const forgotRegisterWrapper = css({
  textAlign: 'center',
  color: 'white',
  fontSize: '.9rem',
});

const anchor = css({
  display: 'inline-block',
  textAlign: 'center',
  fontSize: 13,
  fontWeight: 600,
  color: Colors.blue,
});

export default Login;