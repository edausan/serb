import React, { useEffect } from 'react';
import GridWrapper from '../Wrappers/GridWrapper';
import Button from '../Form/Button';
import { Link } from 'react-router-dom'
import { css } from 'glamor';

const Successful = () => {

  useEffect(() => {
    document.title = 'Registration Successful!'
  }, [])

  return (
    <GridWrapper styles={{ minHeight: '100vh', placeContent: 'center', textAlign: 'center', color: 'white' }}>
      <h3 style={{ margin: 0, fontSize: 30 }}>Registration Successful!</h3>
      <section className={btnWrapper}>
        <Link to="/">
          <Button text="LOG IN" />
        </Link>
      </section>
    </GridWrapper>
  );
}


const btnWrapper = css({
  marginTop: 30,
  '> a': {
    textDecoration: 'none'
  }
})

export default Successful;