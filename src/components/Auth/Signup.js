import React, { useState, useEffect } from 'react';
import GridWrapper from '../Wrappers/GridWrapper';
import Input from '../Form/Input';
import Logo from '../Styles/Logo';
import StrengthBar from '../Form/StrengthBar';
import UploadID from '../Form/UploadID';
import { css } from 'glamor';
import Button from '../Form/Button';
import { Colors } from '../Styles/Colors';
import useValidation from '../custom-hooks/useValidation';
import { Link } from 'react-router-dom'

const SignUp = () => {
  const initialState = {
    email: '',
    password: '',
    confirm_password: '',
    first_id: '',
    second_id: '',
  }

  const formErrorState = {
    email: false,
    password: false,
    confirm_password: false,
    formValid: true
  }

  const btnDefaultText = 'CREATE ACCOUNT'
  const [user, setUser] = useState(initialState);
  const [formError, setformError] = useState(formErrorState);
  const [passwordMatch, setPasswordMatch] = useState(true);
  const [noID, setNoID] = useState(false);
  const [btnText, setBtnText] = useState(btnDefaultText);
  const [locked, setLocked] = useState(false);

  useEffect(() => {
    document.title = 'SERB | Sign Up'
  }, [formError])

  const _onChangeHandler = (e) => {
    const id = e.target.id.split('-').join('_');
    setUser({ ...user, [id]: e.target.value });
    setformError(useValidation(user));
  }

  const _checkPasswordMatch = () => {
    if (user.password === user.confirm_password) {
      setPasswordMatch(true)
    } else {
      setPasswordMatch(false)
      formError.password = true
      formError.confirmPassword = true
    }
  }

  const _checkIDs = () => {
    if (user.first_id && user.second_id) {
      return false
    }
    return true
  }

  const _onSubmitHandler = (e) => {
    e.preventDefault();
    setformError(useValidation(user));
    _checkPasswordMatch();
    const no_id = _checkIDs();
    setNoID(no_id)
    _uploadUser(formError.formValid, no_id);
  }


  const _uploadUser = (formValid, noid) => {
    if (formValid && !noid) {
      setLocked(true);
      setBtnText('PLEASE WAIT...');
      localStorage.setItem('user', JSON.stringify(user));
      setTimeout(() => {
        window.location.href = '/registration_successful'
      }, 1000);
    } else {
      setBtnText(btnDefaultText);
    }
  }


  const setFileUpload = (id, value) => {
    setUser({ ...user, [id]: value });
  }

  return (
    <GridWrapper >
      <Logo styles={{ padding: '50px 0' }} />
      <form onSubmit={_onSubmitHandler}>
        <Input locked={locked} error={formError.email} value={user.email} onChange={_onChangeHandler} placeholder="Email Address" type="email" id="email" />

        <Input locked={locked} error={formError.password} value={user.password} onChange={_onChangeHandler} placeholder="Password" type="password" id="password" />

        {user.password.length > 0 && <StrengthBar value={user.password.length} />}

        <Input locked={locked} error={formError.confirmPassword} value={user.confirm_password} onChange={_onChangeHandler} placeholder="Confirm Password" type="password" id="confirm-password" />

        {user.confirm_password.length > 0 && <StrengthBar value={user.confirm_password.length} />}

        {
          !passwordMatch && <label htmlFor="" className={passMatch}>Password doesn't match.</label>
        }

        <section className={uploadWrapper}>
          <p style={{ fontSize: 15 }}>Upload 2 valid IDs</p>
          <GridWrapper template="1fr 1fr" styles={{ maxWidth: 300, margin: '0 auto' }}>
            <UploadID locked={locked} error={noID} id="first-id" setFile={setFileUpload} />
            <UploadID locked={locked} error={noID} id="second-id" setFile={setFileUpload} />
          </GridWrapper>
        </section>
        <Button text={btnText} />
        <span className={loginLink}><Link to="/">LOG IN</Link></span>
      </form>
    </GridWrapper>
  );
}

const loginLink = css({
  marginTop: 50,
  display: 'block',
  textAlign: 'center',
  '> a': {
    textDecoration: 'none',
    color: Colors.blue,
    fontSize: '.9rem',
    fontWeight: 600
  }
});

const passMatch = css({
  color: 'white',
  display: 'block',
  textAlign: 'center',
  marginTop: 20,
  background: Colors.red,
  padding: 5,
  borderRadius: 5
});

const uploadWrapper = css({
  textAlign: 'center',
  color: 'white',
  marginTop: 50,
  marginBottom: 20
});

export default SignUp;