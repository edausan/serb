import React, { useState, useEffect } from 'react';
import Login from './components/Auth/Login';
import { css } from 'glamor';
import { BrowserRouter as Router, Route } from "react-router-dom";
import SignUp from './components/Auth/Signup';
import Successful from './components/Auth/Successful';
import Dashboard from './components/pages/dashboard';
import { AuthContext } from './context/AuthContext';
import ForgotPassword from './components/Auth/ForgotPassword';

function App() {
  const [localUser, setLocalUser] = useState({});

  const _logout = () => {
    localStorage.setItem('loggedin', false);
    console.log('logged out!');
  }

  useEffect(() => {
    const localUser = JSON.parse(localStorage.getItem('user'));
    setLocalUser(localUser);
  }, [])

  return (
    <AuthContext.Provider value={{
      user: localUser,
      isLoggedin: JSON.parse(localStorage.getItem('loggedin')) ? true : false,
      logout: _logout
    }}>
      <div className={wrapper} >
        <Router>
          <Route path="/" exact component={Login} />
          <Route path="/signup" component={SignUp} />
          <Route path="/registration_successful" component={Successful} />
          <Route path="/forgotpassword" component={ForgotPassword} />
          <Route path="/dashboard" component={Dashboard} />
        </Router>
      </div>
    </AuthContext.Provider>
  );
}

const wrapper = css({
  minHeight: '100vh',
  maxHeight: '100vh',
  padding: '0 40px',
  boxSizing: 'border-box',
});

export default App;
